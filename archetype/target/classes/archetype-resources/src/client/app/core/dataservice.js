(function() {
  'use strict';

  angular
    .module('app.core')
    .factory('dataservice', dataservice);

  dataservice.$inject = ['$http', '$q', 'exception', 'logger'];
  /* @ngInject */
  function dataservice($http, $q, exception, logger) {
    var service = {
      getFruit: getFruit,
    };

    return service;

    function getFruit($resource) {
      return $http({
        url: 'rest/fruit',
        method: 'GET',
        params: {},
        isArray: false
      })
        .then(success)
        .catch(fail);

      function success(response) {
        return response.data;
      }

      function fail(e) {
        return exception.catcher('XHR Failed for getFruit')(e);
      }
    }

  }
})();
