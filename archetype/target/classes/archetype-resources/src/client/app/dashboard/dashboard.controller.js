(function() {
  'use strict';

  angular
    .module('app.dashboard')
    .controller('DashboardController', DashboardController);

  DashboardController.$inject = ['$q', 'dataservice', 'logger'];
  /* @ngInject */
  function DashboardController($q, dataservice, logger) {
    var vm = this;
    vm.firstname = 'orange';

    function getFruit() {
      return dataservice.getFruit().then(function(data) {
        vm.firstname = data.fruitName;
        if(vm.firstname=="apple") {
          vm.up=true;
        }
        return vm.firstname;
      });
    }

    getFruit();
  }
})();
