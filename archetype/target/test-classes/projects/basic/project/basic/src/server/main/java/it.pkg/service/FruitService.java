package it.pkg.service;

import it.pkg.domain.Fruit;
import java.sql.*;

public class FruitService {
    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/EMP";

    //  Database credentials
    static final String USER = "sys";
    static final String PASS = "vagrant";

    public Fruit getDefaultFruit() {
        Fruit fruit = new Fruit();
        fruit.setFruitName("Orange");
        fruit.setFruitColor("Orange");
        return fruit;
    }

    public Fruit queryDatabase() {
        Connection conn = null;
        Statement stmt = null;
        Fruit fruit = new Fruit();

        try{
            //STEP 2: Register JDBC driver
//            Class.forName("com.mysql.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to database...");
            String driverName = "oracle.jdbc.driver.OracleDriver";
            Class.forName(driverName).newInstance();
            String nameForConnect = "sys as sysdba";
            String pass = "vagrant";
            String url = "jdbc:oracle:thin:@localhost:1521:ORCL";
//            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            conn = DriverManager.getConnection(url, nameForConnect, pass);

            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT fruitname, color FROM Fruits";
            ResultSet rs = stmt.executeQuery(sql);

            //STEP 5: Extract data from result set
            while(rs.next()){
                //Retrieve by column name
                String fruitName = rs.getString("fruitname");
                String color = rs.getString("color");

                //Display values
//                System.out.print("ID: " + id);
//                System.out.print(", Age: " + age);
//                System.out.print(", First: " + first);
//                System.out.println(", Last: " + last);
                fruit.setFruitName(fruitName);
                fruit.setFruitColor(color);
            }
            //STEP 6: Clean-up environment
            rs.close();
            stmt.close();
            conn.close();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    stmt.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }//end try
        System.out.println("Goodbye!");
        return fruit;
    }
}
