# Orange Juice

A Maven archetype for generating a simple stack

1) install the project in your custom vagrant box:

https://drive.google.com/open?id=0Bz3jqwcSQ8MKM0lERG9tNDNlcm8

`vagrant box add centos7oracle centos7oracle.box`

`vagrant init centos7oracle`

Edit the Vagrantfile and replace it with the [one provided](https://bitbucket.org/jackmertens/orange-juice/src/a096d011087c86d83a2df3768ae4d09eaf806683/Vagrantfile?fileviewer=file-view-default) in this project.

Finally:

`vagrant up`

`vagrant ssh`

`cd /vagrant`

This is the location of the shared directory--useful for opening your project in an external IDE.

`git clone git@bitbucket.org:jackmertens/orange-juice.git`

This part may not work yet - it's configured for my ssh key. May have to clone project external of the box into the shared directory.

Navigate to the root of the archetype directory and execute the following. This installs maven on your local machine.

`mvn clean install`

2) Generate the archetype:

`mvn archetype:generate -DgroupId=<GROUP_ID> -DartifactId=<PROJECT_NAME> -DarchetypeGroupId=fourtwosix -DarchetypeArtifactId=orange-juice-archetype -DinteractiveMode=false`

3) cd into the new project. There should be a pom file in the root directory. Run the following command to package the project into a war and deploy to tomcat:

`bower install`

`mvn clean install org.codehaus.cargo:cargo-maven2-plugin:run`
