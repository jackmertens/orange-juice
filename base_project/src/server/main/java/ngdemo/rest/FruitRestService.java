package ngdemo.rest;

import ngdemo.service.FruitService;
import ngdemo.domain.Fruit;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("/fruit")
public class FruitRestService {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Fruit getDefaultUserInJSON() {
        FruitService fruitService = new FruitService();
        return fruitService.queryDatabase();
    }
}
